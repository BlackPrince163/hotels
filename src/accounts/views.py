from django.http import HttpResponseRedirect
from django.views.generic import FormView

from .forms import RegisterForm


class RegisterView(FormView):
    form_class = RegisterForm
    template_name = "registration/sign_up.html"
    success_url = "/admin"

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.is_staff = True
        instance.is_superuser = True
        instance.save()

        return HttpResponseRedirect(self.get_success_url())
