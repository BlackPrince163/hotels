from django.contrib import admin
from django.urls import path

from .views import HotelListApiView

urlpatterns = [
    path("hotels/", HotelListApiView.as_view({'get': 'list'}), name="hotel"),
]
