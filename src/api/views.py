from rest_framework import mixins, viewsets

from .models import Hotel
from .serializers import HotelSerializer


class HotelListApiView(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = HotelSerializer
    queryset = Hotel.objects.select_related("city").order_by('id')

    def list(self, request, *args, **kwargs):
        if request.GET.get("city_id"):
            try:
                city_id = request.GET.get("city_id")
                self.queryset = self.queryset.filter(city_id=city_id)
            except ValueError:
                pass

        if request.GET.get("from_id"):
            try:
                from_id = request.GET.get("from_id")
                self.queryset = self.queryset.filter(id__gt=from_id)
            except ValueError:
                pass

        if request.GET.get("limit"):
            try:
                limit = int(request.GET.get("limit"))
                self.queryset = self.queryset[:limit]
            except ValueError:
                pass
        return super().list(request, *args, **kwargs)
