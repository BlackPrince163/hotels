from rest_framework import serializers

from .models import Hotel


# class CitySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = City
#         fields = "__all__"


class HotelSerializer(serializers.ModelSerializer):
    # city = CitySerializer()
    city_id = serializers.SerializerMethodField()

    class Meta:
        model = Hotel
        fields = ("id", "name", "address", "city_id", "phone_number", )

    def get_city_id(self, obj):
        return obj.city.id
