from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class City(models.Model):
    title = models.CharField(_("Название города"), max_length=128)

    def __str__(self):
        return self.title


class Hotel(models.Model):
    name = models.CharField(_("Название отеля"), max_length=128)
    address = models.CharField(_("Адрес"), max_length=255)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Номер телефона необходимо вводить в формате: «+999999999». Допускается до 15 цифр.")
    phone_number = models.CharField(_("Номер телефона"), validators=[phone_regex], max_length=17)
    city = models.ForeignKey(City, verbose_name=_("Город"), on_delete=models.CASCADE)

    def __str__(self):
        return self.name
