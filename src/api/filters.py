class HotelFilter(FilterSet):
    class Meta:
        model = User
        fields = {
            "last_temperature": ["lte", "gte"],
        }
